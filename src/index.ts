import { createServer } from 'http';
import url from 'url';
import { BAD_REQUEST, OK, INTERNAL_SERVER_ERROR } from 'http-status'
import search from './core';
import FindMyWay from 'find-my-way'
import { RequestFailedError } from './common';


const router = FindMyWay();

router.on('GET', '/search', async (req, res) => {
  res.setHeader('Access-Control-Allow-Origin', '*');

  const parsedUrl = url.parse(req.url ?? '/', true);
  const query = parsedUrl.query;

  if (!['iterations', 'term'].every((x) => Object.keys(query).includes(x))) {
    res.statusCode = BAD_REQUEST;
    res.end(`{"error":"both 'iterations' and 'term' query parameters are required"}`)
    return;
  }

  const iterations = query['iterations']!;
  if (!Number.isSafeInteger(Number(iterations)) || Array.isArray(iterations)) {
    res.statusCode = BAD_REQUEST;
    res.end(`{"error":"provided 'iterations' is not a valid number"}`)
    return;
  }

  const term = query['term']!;
  if (Array.isArray(term)) {
    res.statusCode = BAD_REQUEST;
    res.end(`{"error":"provided 'term' is not valid"}`)
    return;
  }

  const { code, body } = await async function() {
    try {
      const results = await search(term, parseInt(iterations, 10))
      return {
        code: OK,
        body: JSON.stringify(results),
      };
    } catch (error) {
      if (error instanceof RequestFailedError) {
        return {
          code: error.code,
          body: error.message,
        };
      }

      return {
        code: INTERNAL_SERVER_ERROR,
        body: 'unknown error occurred'
      }
    }
  }()
  res.statusCode = code;
  res.end(body);
})

const server = createServer((req, res) => {
  router.lookup(req, res)
})

function start() {
  server.on('error', (error) => {
      if (error) throw error;
  });
  server.listen(3000, '127.0.0.1', () => {
    console.log('Server listening on: http://localhost:3000');

    console.log(router.prettyPrint({ commonPrefix: true, includeMeta: true }))
  });
}

if (require.main === module) {
  start();
}