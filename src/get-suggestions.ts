import got from "got/dist/source";
import { RequestFailedError } from "./common";


function buildURL(term: string): string {
  return `http://suggestqueries.google.com/complete/search?client=firefox&q=${encodeURIComponent(term)}`;
}

export default async function getSuggestions(term: string): Promise<ReadonlyArray<string>> {
  return got(buildURL(term), {
    headers: {
      accept: "*/*",
      "accept-language": "en-US,en;q=0.9",
      "cache-control": "no-cache",
      pragma: "no-cache",
      "sec-fetch-dest": "empty",
      "sec-fetch-mode": "cors",
      "sec-fetch-site": "same-origin",
      "sec-gpc": "1",
      referrer: "https://www.google.com/",
      referrerPolicy: "origin",
    },
    retry: 3,
  })
    .json<string[][]>()
    .catch((error) => {
      throw new RequestFailedError(error.response.status, error.response.statusText);
    })
    .then((response) => response[1] ?? [])
}