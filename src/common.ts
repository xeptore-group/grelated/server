export class RequestFailedError extends Error {
  constructor(
    public readonly code: number,
    public override readonly message: string,
  ) {
    super(message);
  }
}