import series from "p-series";
import getRelatedSearches from "./get-related-searches";
import getSuggestions from "./get-suggestions";


export type Suggestions = ReadonlyArray<string>;

export type RelatedSearches = ReadonlyArray<string>;

export interface SearchResult {
  readonly relatedSearches: RelatedSearches;
  readonly suggestions: Suggestions;
}

async function searchSuggestions(term: string, iterations: number): Promise<Suggestions> {
  let suggestions = [term];
  for (let i = 0; i < iterations; i++) {
    const x = await Promise.all(suggestions.map((t) => getSuggestions(t)));
    suggestions = Array.from(new Set(x.flat(1)));
  }

  return suggestions
}

async function searchRelatedSearches(term: string, iterations: number): Promise<RelatedSearches> {
  let relatedSearches = [term];
  for (let i = 0; i < iterations; i++) {
    const searchResults = await series(relatedSearches.map((t) => () => getRelatedSearches(t)));
    relatedSearches = Array.from(new Set(searchResults.flat(1)));
  }
  return relatedSearches;
}

export default async function search(term: string, iterations: number): Promise<SearchResult> {
  const [suggestions, relatedSearches] = await Promise.all([
    searchSuggestions(term, iterations),
    searchRelatedSearches(term, iterations),
  ]);

  return {
    relatedSearches,
    suggestions,
  };
}

