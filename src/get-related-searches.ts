// import { RequestFailedError } from "./common";
import { load } from 'cheerio';
import got from "got/dist/source";


const RELATED_SEARCH_ITEM_QUERY_SELECTOR_PATH = 'div.gGQDvd.iIWm4b>a.Q71vJc>div.kjGX2>span>div.BNeawe.s3v9rd.AP7Wnd.lRVwie>span';

function buildURL(term: string): string {
  return `https://www.google.com/search?q=${encodeURIComponent(term)}&gbv=1`;
}

export default async function getRelatedSearches(term: string): Promise<ReadonlyArray<string>> {
  return got(buildURL(term), {
    headers: {
      "referrer": "https://www.google.com/",
      "referrerPolicy": "origin",
      "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
      "Accept-Encoding": "gzip, deflate, br",
      "Accept-Language": "en-US,en;q=0.5",
      "Cache-Control": "no-cache",
      "Connection": "keep-alive",
      "DNT": "1",
      "Host": "www.google.com",
      "Pragma": "no-cache",
      "Sec-Fetch-Dest": "document",
      "Sec-Fetch-Mode": "navigate",
      "Sec-Fetch-Site": "same-origin",
      "Sec-Fetch-User": "?1",
      "Sec-GPC": "1",
      "TE": "trailers",
      "Upgrade-Insecure-Requests": "1",
      "User-Agent": "Mozilla/5.0 (Macintosh; U; PPC Mac OS X; en) AppleWebKit/125.2 (KHTML, like Gecko) Safari/85.8",
    }
  })
    .then((response) => {
      const $ = load(response.body, {}, true);
      const relatedSearches = $(RELATED_SEARCH_ITEM_QUERY_SELECTOR_PATH).map(function() { return $(this).text(); }).get();
      return relatedSearches;
    })
    .catch((error) => {
      console.log(error);
      // throw new RequestFailedError(error.response.status, error.response.statusText);
      return [];
    })
}