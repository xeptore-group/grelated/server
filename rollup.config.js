import commonjs from '@rollup/plugin-commonjs';
import json from '@rollup/plugin-json';
import { nodeResolve } from '@rollup/plugin-node-resolve';
import replace from '@rollup/plugin-replace';
import strip from '@rollup/plugin-strip';
import path from 'path/posix';
import del from 'rollup-plugin-delete';
import { terser } from 'rollup-plugin-terser';
import createCliEntry from './build-plugins/create-cli-entry';
import executable from './build-plugins/executable';
import { bin as packageBin } from './package.json';


/** @type {import('rollup').RollupOptions} */
export default {
  input: './build/index.js',
  strictDeprecations: true,
  output: {
    file: packageBin,
    format: 'cjs',
  },
  plugins: [
    del({
      targets: path.dirname(packageBin),
      verbose: true,
    }),
    json(),
    nodeResolve({
      browser: false,
    }),
    commonjs(),
    strip({
      debugger: false,
      functions: ['assert.*'],
    }),
    replace({
      'process.env.VERSION': process.env.npm_package_version,
      preventAssignment: true,
    }),
    terser({
      format: {
        comments: false,
      },
    }),
    createCliEntry(),
    executable(),
  ],
  treeshake: {
    moduleSideEffects: false,
    propertyReadSideEffects: false,
    tryCatchDeoptimization: false,
  },
};
