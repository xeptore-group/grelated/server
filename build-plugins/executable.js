import { chmodSync } from 'fs';


export default function executable() {
  return {
    name: 'executable',
    writeBundle: options => {
      chmodSync(options.file, '0755');
    },
  };
}
