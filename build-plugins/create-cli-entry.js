// eslint-disable-next-line import/no-extraneous-dependencies
import MagicString from 'magic-string';


export default function createCliEntry() {
  return {
    name: 'create-cli-entry',
    renderChunk(code, chunkInfo) {
      if (chunkInfo.isEntry) {
        const magicString = new MagicString(code);
        return {
          code: magicString.prepend('#!/usr/bin/env node\n').toString(),
          map: magicString.generateMap({ hires: true }),
        };
      }

      return {};
    },
  };
}
